import React, { Component } from 'react';
import axios from 'axios';
import '../App.css';
const USER = 'gtfarng';


class Github extends Component {
  constructor(props)
    {
        super(props)
        this.state = {  data: {} }
    }

    componentDidMount=()=>
    {
        axios.get(`https://api.github.com/users/${USER}`)
            .then(response => 
            {
                    this.setState({data : response.data})
                    console.log(response.data)
            })
    }

    render() 
    {
        const dataOption = Object.keys(this.state.data)
            .map( (key,index) =>
                <option value={index}>
                    {index+1 +'. ' +key+ ': '  + this.state.data[key]}
                </option>
            )

        return (
            <div align="center">
            
                    <h1>My Github Profile</h1>
                    <img src={this.state.data.avatar_url} alt="avatar" width="150px"/> <br/>
                    <ul>
                       <h3 align="left"> 

                        <li>User : {this.state.data.login}</li>
                        <li>Name : {this.state.data.name}</li>
                        <li>Link : {this.state.data.url}</li>
                        <li>Website : {this.state.data['blog']}</li> </h3>
                    </ul>

                    <dd><select>{dataOption}</select></dd>
                    <br/><br/>

                   
            </div>
        );
    }
}

export default Github;
