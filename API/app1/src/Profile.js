import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

const USER = 'gtfarng';

class Profile extends Component {

    constructor(props)
    {
        super(props)
        this.state = {  data: {} }
    }

    componentDidMount=()=>
    {
        axios.get(`https://api.github.com/users/${USER}`)
            .then(response => 
            {
                    this.setState({data : response.data})
                    console.log(response.data)
            })
    }

    render() 
    {
        const dataOption = Object.keys(this.state.data)
            .map( (key,index) =>
                <option value={index}>
                    {index+1 +'. ' +key+ ': '  + this.state.data[key]}
                </option>
            )

        return (
            <div align="center" class="App-header">
            
                    <h1 class="App-link">My Github Profile</h1>
                    <img src={this.state.data.avatar_url} alt="avatar" width="150px"/> <br/>
                    <ul>
                       <h3 align="left"> 

                        <li>User : <a href="{this.state.data.login}">{this.state.data.login}</a></li>
                        <li>Name : <a href="{this.state.data.name}">{this.state.data.name}</a></li>
                        <li>Link : <a href="{this.state.data.url}">{this.state.data.url}</a></li>
                        <li>Website : <a href="{this.state.data.blog}"> {this.state.data['blog']}</a></li> </h3>
                    </ul>

                    <dd><select>{dataOption}</select></dd>
                    <br/><br/>

                   
            </div>
        );
    }
}

export default Profile;